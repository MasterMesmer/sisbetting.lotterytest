﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using sisbetting.lotterytest.DAL;
using sisbetting.lotterytest.DTO;
using sisbetting.lotterytest.Helpers;
using sisbetting.lotterytest.Service;
using System;
using System.Collections.Generic;
using System.Text;

namespace sisbetting.lotterytest.unittests
{
    [TestClass]
    public class LotteryServiceTests
    {
        private static IContextLotteryFactory _contextFactory;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            //Would probably be better to mock ContextFactory/LotteryContext
            string connStr = @"server = localhost;userid=root;pwd=CT!3BzKge28&zn; port=3306; database=test_sislottery; sslmode = none;";
            _contextFactory = new ContextLotteryFactory(connStr);
        }

        [ClassCleanup]
        public static void CleanUp()
        {
            var context = _contextFactory.Create();
            context.Database.EnsureDeleted();
        }

        [TestMethod]
        public void CanCreateADraw()
        {
            IDrawFactory drawFactory = new DrawFactory();
            ILotteryService lotteryService = new LotteryService(_contextFactory, drawFactory);

            var dto = GetDummyDto("created once draw");
            var serviceResult = lotteryService.CreateDraw(dto);

            Assert.IsTrue(serviceResult.Passed);
        }

        [TestMethod]
        public void RejectDuplicateDrawName()
        {
            IDrawFactory drawFactory = new DrawFactory();
            ILotteryService lotteryService = new LotteryService(_contextFactory, drawFactory);

            var dto = GetDummyDto("created duplicate draw");
            lotteryService.CreateDraw(dto);
            var serviceResult = lotteryService.CreateDraw(dto);

            Assert.IsFalse(serviceResult.Passed);

        }

        private DrawDto GetDummyDto(string name)
        {
            DrawDto drawDto = new DrawDto()
            {
                Name = name,
                Description = "Testing only",
                DrawDate = DateTime.Now,
                MaxPrimary = 49,
                MinPrimary = 1,
                MaxSecondary = 10,
                MinSecondary = 1,
                QuantityPrimary = 5,
                QuantitySecondary = 2
            };

            return drawDto;
        }
    }
}
