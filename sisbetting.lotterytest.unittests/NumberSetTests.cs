using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using sisbetting.lotterytest.Model;
using System.Collections.Generic;
using sisbetting.lotterytest.DTO;

namespace sisbetting.lotterytest.unittests
{
    [TestClass]
    public class NumberSetTests
    {
        [TestMethod]
        public void ValidateNumbersInRange()
        {
            NumberSet numberSet = new NumberSet(5, 1, 10);
            var nums = new List<int>() { 1, 5, 7, 8, 4 };

            Assert.IsTrue(numberSet.Validate(nums));

        }

        [TestMethod]
        public void ValidateOverRange()
        {
            NumberSet numberSet = new NumberSet(5, 1, 10);
            var nums = new List<int>() { 1, 5, 7, 8, 12 };

            Assert.IsFalse(numberSet.Validate(nums));

        }

        [TestMethod]
        public void ValidateUnderRange()
        {
            NumberSet numberSet = new NumberSet(5, 1, 10);
            var nums = new List<int>() { 0, 5, 7, 8, 9 };

            Assert.IsFalse(numberSet.Validate(nums));

        }

        [TestMethod]
        public void ValidateWrongQuantityOfNumbers()
        {
            NumberSet numberSet = new NumberSet(5, 1, 10);
            var nums = new List<int>() { 1, 5 };

            Assert.IsFalse(numberSet.Validate(nums));

        }

        [TestMethod]
        public void ValidateWinningNumberDtoSplitStr()
        {
            var numString = "4,5,10";

            var winningNumbers = new WinningNumbersDto { DrawName = "for test only", PrimaryNumbers = numString };

            var result = winningNumbers.GetPrimaryNumbers();

            Assert.AreEqual(4, result[0]);
            Assert.AreEqual(5, result[1]);
            Assert.AreEqual(10, result[2]);
        }

    }
}
