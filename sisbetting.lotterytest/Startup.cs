﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using sisbetting.lotterytest.DAL;
using Microsoft.EntityFrameworkCore;
using sisbetting.lotterytest.Service;
using sisbetting.lotterytest.Helpers;
using Microsoft.Extensions.Logging;

namespace sisbetting.lotterytest
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // Add framework services.
            services.AddMvc();

            services.AddTransient<IDrawFactory, DrawFactory>();
            services.AddScoped<ILotteryService, LotteryService>();

            var connString = Configuration.GetConnectionString("LotteryConnection");
            services.AddSingleton<IContextLotteryFactory>(s => new ContextLotteryFactory(connString));


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();




            //Used for ease of local testing
            if (env.IsDevelopment())
            {
                app.UseCors(options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials());
                app.UseDeveloperExceptionPage();
            }


            app.UseMvc();
        }
    }
}
