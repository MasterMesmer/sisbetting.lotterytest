﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisbetting.lotterytest.Model
{
    public class WinningNumber : BaseEntity
    {
        public virtual NumberSet NumberSet {get; set;}
        public int Value { get; set; }

        public WinningNumber() { }

        public WinningNumber(int value,NumberSet numberSet)
        {
            NumberSet = numberSet;
            Value = value;
        }
    }
}
