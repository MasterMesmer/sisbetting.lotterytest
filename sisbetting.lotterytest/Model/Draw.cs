﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisbetting.lotterytest.Model
{
    public class Draw : BaseEntity
    {
        public String Name { get; set; }
        public String Description { get; set; }
        public  DateTime DrawDate { get; set; }
        public virtual NumberSet PrimaryNumbers { get; set; }
        public virtual NumberSet SecondaryNumbers { get; set; }

        public Draw() { }

        public Draw(String name, String description, DateTime drawDate,NumberSet primary, NumberSet secondary)
        {
            DrawDate = drawDate;
            Description = description;
            Name = name;
            PrimaryNumbers = primary;
            SecondaryNumbers = secondary;
        }
    }
}
