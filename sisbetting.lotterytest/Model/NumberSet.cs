﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisbetting.lotterytest.Model
{
    public class NumberSet : BaseEntity
    {
        

        public int MaxValue { get; set; }
        public int MinValue { get; set; }
        public int AmountOfNumbers { get; set; }

        public NumberSet() { }

        public NumberSet(int quantityPrimary, int minPrimary, int maxPrimary)
        {
            AmountOfNumbers = quantityPrimary;
            MinValue = minPrimary;
            MaxValue = maxPrimary;
        }

        public bool Validate(IList<int> numbers)
        {
            if (numbers.Count() != AmountOfNumbers)
                return false;

            foreach(var number in numbers)
            {
                if (number < MinValue || number > MaxValue)
                    return false;
            }

            return true;
        }
    }
}
