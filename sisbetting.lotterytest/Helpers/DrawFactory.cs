﻿using sisbetting.lotterytest.DTO;
using sisbetting.lotterytest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisbetting.lotterytest.Helpers
{
    public class DrawFactory : IDrawFactory
    {
        public Draw CreateFromDto(DrawDto dto)
        {
            var primaryNumbers = new NumberSet(dto.QuantityPrimary, dto.MinPrimary, dto.MaxPrimary);
            var secondaryNumbers = new NumberSet(dto.QuantitySecondary, dto.MinSecondary, dto.MaxSecondary);

            var draw = new Draw(dto.Name, dto.Description, dto.DrawDate, primaryNumbers, secondaryNumbers);
            return draw;
        }
    }
}
