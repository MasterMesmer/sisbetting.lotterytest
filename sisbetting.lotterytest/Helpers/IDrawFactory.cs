﻿using sisbetting.lotterytest.DTO;
using sisbetting.lotterytest.Model;

namespace sisbetting.lotterytest.Helpers
{
    public interface IDrawFactory
    {
        Draw CreateFromDto(DrawDto dto);
    }
}