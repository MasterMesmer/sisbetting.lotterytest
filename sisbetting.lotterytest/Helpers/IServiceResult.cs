﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisbetting.lotterytest.Helpers
{
    public interface IServiceResult
    {
        Boolean Passed { get; }
        String Msg { get; }
    }
}
