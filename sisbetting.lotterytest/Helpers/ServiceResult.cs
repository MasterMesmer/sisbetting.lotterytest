﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisbetting.lotterytest.Helpers
{
    public class ServiceResult : IServiceResult
    {
        public readonly bool _result;
        public readonly string _msg;

        public ServiceResult(bool result,string msg)
        {
            _result = result;
            _msg = msg;
        }

        public bool Passed => _result;
        public string Msg => _msg;

    }
}
