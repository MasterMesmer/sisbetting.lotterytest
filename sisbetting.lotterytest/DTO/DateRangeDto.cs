﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisbetting.lotterytest.DTO
{
    public class DateRangeDto
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public bool Validate()
        {
            if (Start < End)
                return true;
            else
                return false;
        }
    }
}
