﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisbetting.lotterytest.DTO
{
    public class WinningNumbersDto
    {
        public String DrawName { get; set; }
        public String PrimaryNumbers { get; set; }
        public String SecondaryNumbers { get; set; }
        

        private List<int> Parse(String numberString)
        {
            var numbers = numberString.Split(',');

            List<int> numberInts = new List<int>();

            foreach (var numStr in numbers)
            {
                numberInts.Add(Convert.ToInt16(numStr));
            }

            return numberInts;
        }
        
        public List<int> GetPrimaryNumbers()
        {
            return Parse(PrimaryNumbers);
        }

        public List<int> GetSecondaryNumbers()
        {
            return Parse(SecondaryNumbers);
        }
    }
}
