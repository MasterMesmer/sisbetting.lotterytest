﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisbetting.lotterytest.DTO
{
    public class DrawDto
    {
        public String Name { get; set; }
        public String Description { get; set; }
        public DateTime DrawDate { get; set; }
        public int MaxPrimary { get; set; }
        public int MinPrimary { get; set; }
        public int QuantityPrimary { get; set; }
        public int MaxSecondary { get; set; }
        public int MinSecondary { get; set; }
        public int QuantitySecondary { get; set; }
    }
}
