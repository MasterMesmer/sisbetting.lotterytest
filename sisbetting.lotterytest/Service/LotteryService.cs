﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using sisbetting.lotterytest.DTO;
using sisbetting.lotterytest.Helpers;
using sisbetting.lotterytest.Model;
using sisbetting.lotterytest.DAL;
using Microsoft.EntityFrameworkCore;

namespace sisbetting.lotterytest.Service
{
  
    public class LotteryService : ILotteryService
    {
        private readonly IContextLotteryFactory _contextFactory;
        private readonly IDrawFactory _drawFactory;

        public LotteryService(IContextLotteryFactory contextFactory, IDrawFactory drawFactory)
        {
            _contextFactory = contextFactory;
            _drawFactory = drawFactory;
        }

        public IServiceResult AddWinningNumbers(WinningNumbersDto winningNumbers)
        {
            var context = _contextFactory.Create();

            var draw = context.Draws.Where(c => c.Name.Equals(winningNumbers.DrawName))
                .Include(p => p.PrimaryNumbers)
                .Include(s => s.SecondaryNumbers)
                .SingleOrDefault();
                

            if (draw == null)
                return new ServiceResult(false,"Draw Name not found");

           
            var primaryResult = SaveNumbersToNumberSet(draw.PrimaryNumbers, winningNumbers.GetPrimaryNumbers(), context);
            var secondaryResult = SaveNumbersToNumberSet(draw.SecondaryNumbers, winningNumbers.GetSecondaryNumbers(), context);

            if(primaryResult && secondaryResult)
            {
                return new ServiceResult(true, "Winning Numbers added to draw");
            }
            else
            {
                return new ServiceResult(false, "numbers valid validation");
            }
            
            
        }

        private bool SaveNumbersToNumberSet(NumberSet numberSet, IList<int> winningNumbers, LotteryContext context)
        {
            var result = numberSet.Validate(winningNumbers);

            if (!result)
                return false;

            foreach (var number in winningNumbers)
            {
                var winningNumber = new WinningNumber(number, numberSet);
                context.WinningNumbers.Add(winningNumber);
            }

            context.SaveChanges();

            return true;
        }



        public IServiceResult CreateDraw(DrawDto drawDto)
        {
            var context = _contextFactory.Create();

            //check that name is not already used in db.
            var existingDraws = context.Draws.Where(c => c.Name.Equals(drawDto.Name));

            if (existingDraws.Count() > 0)
                return new ServiceResult(false,"This draw already exists");

            var draw = _drawFactory.CreateFromDto(drawDto);
            context.Draws.Add(draw);
            context.SaveChanges();

            return new ServiceResult(true,"Draw created");
        }

        public IServiceResult FindDrawsInDateRange(DateRangeDto dateRange, out IEnumerable<Draw> matchingDraws)
        {

            // Bit picky but checking that start time isn't after end time
            if (!dateRange.Validate())
            {
                matchingDraws = null;
                return new ServiceResult(false,"Start date was after end date");
            }

            var context = _contextFactory.Create();

            matchingDraws = context.Draws
                .Where(c => (c.DrawDate > dateRange.Start) && (c.DrawDate < dateRange.End))
                .Include(p => p.PrimaryNumbers)
                .Include(s => s.SecondaryNumbers);


            return new ServiceResult(true,"Draws returned");
        }
    }
}
