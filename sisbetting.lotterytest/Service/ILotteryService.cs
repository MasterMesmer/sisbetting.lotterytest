﻿using sisbetting.lotterytest.DTO;
using sisbetting.lotterytest.Helpers;
using sisbetting.lotterytest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisbetting.lotterytest.Service
{
    public interface ILotteryService
    {
        IServiceResult CreateDraw(DrawDto drawDto);
        IServiceResult AddWinningNumbers(WinningNumbersDto winningNumbers);
        IServiceResult FindDrawsInDateRange(DateRangeDto dateRange, out IEnumerable<Draw> draws);
    }
}
