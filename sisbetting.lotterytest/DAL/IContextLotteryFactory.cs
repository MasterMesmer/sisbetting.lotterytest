﻿namespace sisbetting.lotterytest.DAL
{
    public interface IContextLotteryFactory
    {
        LotteryContext Create();
    }
}