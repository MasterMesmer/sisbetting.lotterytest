﻿using Microsoft.EntityFrameworkCore;
using sisbetting.lotterytest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace sisbetting.lotterytest.DAL
{
    public class LotteryContext : DbContext, ILotteryContext
    {
        public DbSet<Draw> Draws { get; set; }
        public DbSet<NumberSet> NumberSets { get; set; }
        public DbSet<WinningNumber> WinningNumbers { get; set; }

        public LotteryContext(DbContextOptions<LotteryContext> options) : base(options)
        {
        }

    }
}
