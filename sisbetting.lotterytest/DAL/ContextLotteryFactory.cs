﻿using Microsoft.EntityFrameworkCore;
using MySQL.Data.EntityFrameworkCore.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisbetting.lotterytest.DAL
{
    public class ContextLotteryFactory : IContextLotteryFactory
    {
        private readonly string _connString;

        public ContextLotteryFactory(string connString)
        {
            _connString = connString;

            // Ensures Database is created once at startup 
            var optionsBuilder = new DbContextOptionsBuilder<LotteryContext>();
            optionsBuilder.UseMySQL(_connString);

            //Ensure database creation
            var context = new LotteryContext(optionsBuilder.Options);
            context.Database.EnsureCreated();
        }

         public  LotteryContext Create()
            {
                var optionsBuilder = new DbContextOptionsBuilder<LotteryContext>();
                optionsBuilder.UseMySQL(_connString);

                var context = new LotteryContext(optionsBuilder.Options);

                return context;
            }
        
    }
}
