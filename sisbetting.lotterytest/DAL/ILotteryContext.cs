﻿using Microsoft.EntityFrameworkCore;
using sisbetting.lotterytest.Model;

namespace sisbetting.lotterytest.DAL
{
    public interface ILotteryContext
    {
        DbSet<Draw> Draws { get; set; }
        DbSet<NumberSet> NumberSets { get; set; }
        DbSet<WinningNumber> WinningNumbers { get; set; }
        int SaveChanges();
    }
}