﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using sisbetting.lotterytest.Service;
using sisbetting.lotterytest.Model;
using Newtonsoft.Json;
using sisbetting.lotterytest.DTO;
using Microsoft.Extensions.Logging;

namespace sisbetting.lotterytest.Controllers
{
    [Route("api/v1/draw")]
    public class DrawController : BaseController
    {
        private readonly ILogger<DrawController> _logger;

        public DrawController(ILotteryService lotteryService,ILogger<DrawController> logger) : base(lotteryService)
        {
            _logger = logger;
        }

        // GET api/values/5
        [HttpGet]
        public IActionResult Get(DateRangeDto dateRange)
        {

            IEnumerable<Draw> draws = null;
            var result = _lotteryService.FindDrawsInDateRange(dateRange, out draws);

            if (!result.Passed)
            {
                _logger.LogWarning("Get dates failed for reason: " + result.Msg);
                return BadRequest(result.Msg);
            }

            
            return Json(draws);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] DrawDto drawDto)
        {
            
            var result = _lotteryService.CreateDraw(drawDto);

            if (!result.Passed)
            {
                _logger.LogWarning("Create draw failed with msg: " + result.Msg);
                return BadRequest(result.Msg);
            }

            return Ok("Draw added:" + drawDto.Name);
        }
        
    }
}
