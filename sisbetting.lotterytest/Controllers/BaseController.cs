﻿using Microsoft.AspNetCore.Mvc;
using sisbetting.lotterytest.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisbetting.lotterytest.Controllers
{
    public abstract class BaseController : Controller
    {
        protected readonly ILotteryService _lotteryService;

        public BaseController(ILotteryService lotteryService)
        {
            _lotteryService = lotteryService;
        }
    }
}
