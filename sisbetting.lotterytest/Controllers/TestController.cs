﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using sisbetting.lotterytest.Service;
using Microsoft.AspNetCore.Mvc;
using sisbetting.lotterytest.DTO;
using Microsoft.Extensions.Logging;

namespace sisbetting.lotterytest.Controllers
{
    public class TestController : BaseController
    {
        private readonly ILogger<TestController> _logger;

        public TestController(ILotteryService lotteryService,ILogger<TestController> logger) : base(lotteryService)
        {
            _logger = logger;
        }

        public IActionResult LogHit()
        {
            _logger.LogInformation("LogHit has been hit");
            return Content("Log hit has been hit");
        }

        public IActionResult Index(string name = "1st Draw")
        {
            DrawDto drawDto = new DrawDto()
            {
                Name = name,
                Description = "A simple test of first draw",
                DrawDate = DateTime.Now,
                MaxPrimary = 49,
                MinPrimary = 1,
                MaxSecondary = 10,
                MinSecondary = 1,
                QuantityPrimary = 5,
                QuantitySecondary = 2
            };

            var result = _lotteryService.CreateDraw(drawDto);

            if(!result.Passed)
                return Content("Failed for reason....");

            return Content("Added lottery: " + drawDto.Name);
        }


    }
}
