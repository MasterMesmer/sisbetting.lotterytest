﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using sisbetting.lotterytest.Service;
using sisbetting.lotterytest.DTO;
using Microsoft.Extensions.Logging;

namespace sisbetting.lotterytest.Controllers
{
    [Route("api/v1/winningnumber")]
    public class WinningNumberController : BaseController
    {
        private readonly ILogger<WinningNumberController> _logger;

        public WinningNumberController(ILotteryService lotteryService,ILogger<WinningNumberController> logger) : base(lotteryService)
        {
            _logger = logger;
        }

        [HttpPost]
        public IActionResult Post([FromBody] WinningNumbersDto winningNumberDto)
        {
            var result = _lotteryService.AddWinningNumbers(winningNumberDto);

            if (!result.Passed)
            {
                _logger.LogWarning("Add draw failed with message" + result.Msg);
                return BadRequest(result.Msg);
            }

            return Ok(result.Msg);
        }


    }
}
