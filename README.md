# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo is for demo project to demonstrate good coding practice for SIS Betting. As a quick overview the app is built in dotnet core, uses Entity Framework to talk to a MySql database and has an angular app that calls the web API.

### Tools used ###

* Visual Studio 2017 - best for c# programming and lovely IDE
* Visual Studio Code - nice lightweight editor for angular apps, lots of nice code extensions
* Bitbucket - Source control

### Packages ###
* Microsoft.NET.Test.Sdk - Unit Testing
* Microsoft.AspNetCore.* - Packages for making web API in dotnet core project
* Microsoft.EntityFrameworkCore.* - Packages for using Entity Framework as an ORM
* MySql.Data.EntityFrameworkCore - Package for using MySql with EF

### How do I get set up? ###

#### Web API ####
* Load Solution in VS 2017
* run sisbetting.lotterytest (as kestral rather than IIS or else port may not be initiated)
* App should now be running on http://localhost:5050

#### Angular Page ####

* using command line navigate to the angular app folder and type

`ng serve --open`

* The app will now start up and a browser will be opened to few the web page

### Improve with more time ###
* setup app settings to include development, staging, production environment
* Setup authorisation/login managed via login database
* Although used DbContext with other Orm's have impemented Repositories.OfferRepo etc....
* Add security to webapi for authorised endpoints, client Id and client secret key
* Logging is fairly basic and only logs to console, would use something like NLog in full app
* angular app could be much more pretty and user friendly given time (specifically the date time bit is a bit unfriendly)
* winning numbers would be in object model for get and show up in angular app
* Improved validation of DTO's
* Unit tests would use mocking 

### Who do I talk to? ###

* Alex Levitt
