import { WinningNumbersDto } from './dto/WinningNumbersDto';
import { DateRangeDto } from './dto/DateRangeDto';
import { DrawDto } from './dto/DrawDto';
import { LotteryService } from './lottery.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'My First App!!!!!';


  constructor (private lotteryService: LotteryService) {}


}
