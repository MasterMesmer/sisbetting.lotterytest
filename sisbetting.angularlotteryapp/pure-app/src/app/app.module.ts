import { Http } from '@angular/http';
import { LotteryService } from './lottery.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { CreatedrawComponent } from './createdraw/createdraw.component';
import { AddwinningnumbersComponent } from './addwinningnumbers/addwinningnumbers.component';
import { FinddrawsComponent } from './finddraws/finddraws.component';

@NgModule({
  declarations: [
    AppComponent, CreatedrawComponent, AddwinningnumbersComponent, FinddrawsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [LotteryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
