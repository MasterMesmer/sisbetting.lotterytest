import { DateRangeDto } from './../dto/DateRangeDto';
import { LotteryService } from './../lottery.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-finddraws',
  templateUrl: './finddraws.component.html',
  styleUrls: ['./finddraws.component.css']
})
export class FinddrawsComponent implements OnInit {

  msg = '';
  dateRange = new DateRangeDto();
  constructor (private lotteryService: LotteryService) {}
  
  drawResults = ['bob', 'alex', 'james'];

  ngOnInit() {
  }

  onGetDraws() {
    this.lotteryService.getDrawsByDate(this.dateRange)
    .subscribe(
      (response) => {
        this.drawResults = response.json();
     },
      (error) => {this.msg = error.text(); }
      );
  }

}
