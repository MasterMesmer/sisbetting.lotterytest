import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinddrawsComponent } from './finddraws.component';

describe('FinddrawsComponent', () => {
  let component: FinddrawsComponent;
  let fixture: ComponentFixture<FinddrawsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinddrawsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinddrawsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
