import { DrawDto } from './../dto/DrawDto';
import { LotteryService } from './../lottery.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-createdraw',
  templateUrl: './createdraw.component.html',
  styleUrls: ['./createdraw.component.css']
})
export class CreatedrawComponent implements OnInit {

  drawModel = new DrawDto();
  msg = '';

  constructor (private lotteryService: LotteryService) {}

  ngOnInit() {
  }

onContactServer() {
    this.lotteryService.saveDraw(this.drawModel)
    .subscribe(
      (response) => {this.msg = response.text(); },
      (error) => {this.msg = error.text(); }
      );
  }
}
