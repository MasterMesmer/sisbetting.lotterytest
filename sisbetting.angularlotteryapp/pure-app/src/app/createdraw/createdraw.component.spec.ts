import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatedrawComponent } from './createdraw.component';

describe('CreatedrawComponent', () => {
  let component: CreatedrawComponent;
  let fixture: ComponentFixture<CreatedrawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatedrawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatedrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
