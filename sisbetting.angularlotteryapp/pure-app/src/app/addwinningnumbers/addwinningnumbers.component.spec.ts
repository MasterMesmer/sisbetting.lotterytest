import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddwinningnumbersComponent } from './addwinningnumbers.component';

describe('AddwinningnumbersComponent', () => {
  let component: AddwinningnumbersComponent;
  let fixture: ComponentFixture<AddwinningnumbersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddwinningnumbersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddwinningnumbersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
