import { WinningNumbersDto } from './../dto/WinningNumbersDto';
import { LotteryService } from './../lottery.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-addwinningnumbers',
  templateUrl: './addwinningnumbers.component.html',
  styleUrls: ['./addwinningnumbers.component.css']
})
export class AddwinningnumbersComponent implements OnInit {

  winNumbers = new WinningNumbersDto();
  msg = '';

  constructor (private lotteryService: LotteryService) {}

  ngOnInit() {
  }

  addWinningNumbers() {


    this.lotteryService.addWinningNumbersToDraw(this.winNumbers)
    .subscribe(
      (response) => {this.msg = response.text(); },
      (error) => {this.msg = error.text(); }
      );
  }

}
