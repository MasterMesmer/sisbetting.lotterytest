import { WinningNumbersDto } from './dto/WinningNumbersDto';
import { DateRangeDto } from './dto/DateRangeDto';
import { DrawDto } from './dto/DrawDto';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, URLSearchParams, ResponseContentType, RequestOptionsArgs, Headers } from '@angular/http';


@Injectable()
export class LotteryService {

    constructor(private http: Http) { }

    private apiUrl = 'http://localhost:5050/api/v1/';
    private drawEndpoint = 'draw';
    private numberEndpoint = 'winningnumber';

    addWinningNumbersToDraw(winningNumbers: WinningNumbersDto) {

        return this.http.post(this.apiUrl + this.numberEndpoint, winningNumbers);
    }

    saveDraw(drawDto: DrawDto) {
        return this.http.post(this.apiUrl + this.drawEndpoint, drawDto);

    }

    getDrawsByDate(dateRange: DateRangeDto) {

        const params2 = new URLSearchParams();
        params2.append('Start', dateRange.Start);
        params2.append('End', dateRange.End);


        return this.http.get(this.apiUrl + this.drawEndpoint, this.getHeaderOptions(params2));
    }

    getHeaderOptions(searchParams: URLSearchParams) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        const options = new RequestOptions({headers: headers, params: searchParams});
        return options;
    }


}

class GetSettings implements RequestOptionsArgs {

}
