export class WinningNumbersDto {

    public DrawName: string;
    public PrimaryNumbers: string;
    public SecondaryNumbers: string;

}
