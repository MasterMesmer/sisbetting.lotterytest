export class DrawDto {

    public Name: string;
    public Description: string;
    public DrawDate: string;
    public MaxPrimary: number;
    public MinPrimary: number;
    public QuantityPrimary: number;
    public MaxSecondary: number;
    public MinSecondary: number;
    public QuantitySecondary: number;

    constructor() {
        this.Name = 'My second draw dto form A2';
        this.Description = 'A2 is pretty funky!';
        this.DrawDate = '2016-02-04';
        this.MaxPrimary = 49;
        this.MinPrimary = 1;
        this.QuantityPrimary = 5;
        this.MaxSecondary = 10;
        this.MinSecondary = 1;
        this.QuantitySecondary = 2;


    }
}
